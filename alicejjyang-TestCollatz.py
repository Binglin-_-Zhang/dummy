#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1) 
        
    def test_read_3(self):
        s = "1 999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
    # v is maximum cycle length down to 1 from 10
        v = collatz_eval(1, 10)
    # verifies that v is equal to the correct value (fix this!!!)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)
    
    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v = collatz_eval(40, 560)
        self.assertEqual(v, 144)
    
    def test_eval_6(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
        
    def test_eval_7(self):
        v = collatz_eval(200, 100)
        self.assertEqual(v, 125)
        
    def test_eval_8(self):
        v = collatz_eval(50, 250)
        self.assertEqual(v, 128)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 40, 560, 144)
        self.assertEqual(w.getvalue(), "40 560 144\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 200, 100, 125)
        self.assertEqual(w.getvalue(), "200 100 125\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        # makes sure extracted buffer wvalue w is equal to the StringIO
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_2(self):
        r = StringIO("40 560\n1 1\n283 679\n")
        w = StringIO()
        collatz_solve(r, w)
        # makes sure extracted buffer wvalue w is equal to the StringIO
        self.assertEqual(
            w.getvalue(), "40 560 144\n1 1 1\n283 679 145\n")
            
    def test_solve_3(self):
        r = StringIO("666 666\n2 1\n1 500\n")
        w = StringIO()
        collatz_solve(r, w)
        # makes sure extracted buffer wvalue w is equal to the StringIO
        self.assertEqual(
            w.getvalue(), "666 666 114\n2 1 2\n1 500 144\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
